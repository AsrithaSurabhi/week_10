package week_10;

public class Square {
	// properties
	int width;
	int height;
	String colour;
	 
	public Square(int h, int w, String c)	{
		this.height=h;
		this.width=w;
		this.colour=c;
	}
	public void sayHello()
	{
		System.out.println("I'm working!");
		
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	

}
